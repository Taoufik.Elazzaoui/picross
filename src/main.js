import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

// Les balises <router-link> et <router-view> sont mtn dispo
// this.$router et this.route sont aussi dispos dans les scripts
